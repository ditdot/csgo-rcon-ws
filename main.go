package main

import (
	"log"

	"github.com/gofiber/contrib/websocket"
	"github.com/gofiber/fiber/v2"
	"github.com/gorcon/rcon"
)

func main() {
	app := fiber.New()

	app.Use("/ws/rcon", func(c *fiber.Ctx) error {
		if websocket.IsWebSocketUpgrade(c) {
			return c.Next()
		}

		return fiber.ErrUpgradeRequired
	})

	app.Get("/ws/rcon", websocket.New(func(c *websocket.Conn) {
		addr := c.Cookies("Addr")
		passwd := c.Cookies("Passwd")

		var (
			mt   int
			msg  []byte
			conn *rcon.Conn
			err  error
			res  string
		)

		conn, err = rcon.Dial(addr, passwd)
		if err != nil {
			log.Println("dial:", err)
			return
		}
		defer conn.Close()

		for {
			if mt, msg, err = c.ReadMessage(); err != nil {
				log.Println("read:", err)
				break
			}

			log.Printf("recv: %s", msg)

			res, err = conn.Execute(string(msg))
			if err != nil {
				log.Println("execute:", err)
				break
			}

			log.Printf("resp: %s", res)

			if err = c.WriteMessage(mt, []byte(res)); err != nil {
				log.Println("write:", err)
				break
			}
		}

	}))

	log.Fatal(app.Listen(":3000"))
}
